### Hi there 👋
##### Welcome to my GitLab! Where I release the code of projects and configurations of my Arch Linux.
#### If you want make a donation, this is my Monero Wallet:
`8BmkJPyb9Gca1BjgVyDwesYxUidcfRUt99RAp8QdtF7K35pobg8qy9UcFMXecMpxTVXcJo22PKJFTVQSzvPoxQBvJ1LykHJ`

Thank you!

![visitors](https://visitor-badge.laobi.icu/badge?page_id=pedroportales.pedroportales)
[![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=102)](https://github.com/ellerbrock/open-source-badge/)

## 🔧 My current interest in technologies & tools

<img alt="Shell Script" src="https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white"/> <img alt="Python" src="https://img.shields.io/badge/Python-blue.svg?style=for-the-badge&logo=python&logoColor=yellow"/> <img alt="Linux" src="https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black"> <img alt="Git" src="https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white"/> <img alt="GitHub" src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white"/> <img alt="GitLab" src="https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white"/>
